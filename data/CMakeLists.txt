install(FILES xwayland.qtmir.desktop
	DESTINATION ${CMAKE_INSTALL_DATADIR}/applications
)

install(FILES xwayland.qtmir.png
        DESTINATION ${CMAKE_INSTALL_DATADIR}/icons/hicolor/256x256/apps
)
